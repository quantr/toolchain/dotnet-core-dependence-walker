# Introduction

This command can generate dependency graph of dotnet core dll

# Author

Peter <peter@quantr.hk>

# compile

run "sudo npm link", then type in "dotnet-core-dependence-walker"

# run the project

npm run test && npm run dot

# generate graph for a specific dll

dotnet-core-dependence-walker dot /Users/peter/workspace/coreclr/bin/tests/OSX.x64.Debug/Tests/Core_Root System.Console.dll

npm run dot

# generate graph for all dll files in a specific folder

dotnet-core-dependence-walker dot /Users/peter/workspace/coreclr/bin/tests/OSX.x64.Debug/Tests/Core_Root

npm run dot

