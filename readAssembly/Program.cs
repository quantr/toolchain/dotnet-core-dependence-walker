﻿using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace readAssembly
{
	class Program
	{

		static void Main(string[] args)
		{
			if (args[0] == "namespace")
			{
				var files = Directory.GetFiles(args[1], "*.dll", SearchOption.AllDirectories);
				foreach (var file in files)
				{
					string dllFilename = Path.GetFileName(file);
					//Console.WriteLine(dllFilename);
					//Assembly a = Assembly.GetCallingAssembly();// typeof(Program).Assembly;
					try
					{
						// if (dllFilename == "System.Private.CoreLib.dll")
						// {
						Assembly asm = Assembly.LoadFile(file);
						var namespaces = GetNamespacesInAssembly(asm);
						if (namespaces.Count() == 0)
						{
							Console.WriteLine(dllFilename + "----" + asm.FullName.Split(",")[0]);
						}
						else
						{
							foreach (string ns in namespaces.OrderBy(n => n))
							{
								Console.WriteLine(dllFilename + "----" + ns);
							}
						}
						// }
					}
					catch
					{
						Console.WriteLine(dllFilename + "----" + dllFilename.Replace(".dll", ""));
						// Console.WriteLine(" > " + file + ", " + ex.Message);
					}
				}
			}
			else if (args[0] == "extern")
			{
				var files = Directory.GetFiles(args[1], "*.dll", SearchOption.AllDirectories);
				foreach (var file in files)
				{
					string dllFilename = Path.GetFileName(file);
					//Assembly a = Assembly.GetCallingAssembly();// typeof(Program).Assembly;
					try
					{
						// if (dllFilename == "System.Memory.dll")
						// {
						Assembly a = Assembly.LoadFile(file);
						// Console.WriteLine(a.FullName);
						AssemblyName[] names = a.GetReferencedAssemblies();
						// Console.WriteLine(names);
						string temp = "";
						foreach (AssemblyName assemblyName in names)
						{
							temp += assemblyName.Name + ",";
						}
						Console.WriteLine(dllFilename + "----" + temp);
						// }
					}
					catch (Exception ex)
					{
						// Console.WriteLine(ex.Message);
						// Console.WriteLine(dllFilename + "----" + dllFilename.Replace(".dll", ""));
					}
				}
			}
		}

		private static IEnumerable<string> GetNamespacesInAssembly(Assembly asm)
		{
			Type[] types = asm.GetTypes();

			return types.Select(t => t.Namespace)
						.Distinct()
						.Where(n => n != null);
		}
	}
}
