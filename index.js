#!/usr/bin/env node

const yargs = require('yargs');
const {
	exec,
	execSync
} = require('child_process');
const path = require('path');

// const [, , ...args] = process.argv;
// console.log(`dotnet-core-dependence-walker 2 ${args}`);

function findDll(assemblyMap, assemblyName) {
	for (var [key, value] of assemblyMap) {
		// console.log(`com ${value} == ${assemblyName}`);
		// if (value.indexOf(assemblyName) != -1) {
		// 	return key;
		// }

		if (key == assemblyName) {
			return value;
		}
	}
	return null;
}

// function getMaxDepth(dependencyMap) {
//   var depth = -9999;
//   for (var [key, value] of dependencyMap) {
//     // console.log('key=' + key);
//     var processedList = [key];
//     temp = getMaxDepthChild(dependencyMap, key, 1, processedList)
//     // console.log('temp=' + temp);
//     if (temp > depth) {
//       depth = temp;
//     }
//   }
//   return depth;
// }

// function getMaxDepthChild(dependencyMap, key, depth, processedList) {
//   var childList = dependencyMap.get(key);
//   // console.log("\t".repeat(depth) + key + ", " + childList);
//   if (childList != null) {
//     for (var x = 0; x < childList.length; x++) {
//       var item = childList[x];
//       // console.log("\t\t".repeat(depth) + key + ", " + item);
//       if (processedList.indexOf(item) == -1) {
//         processedList.push(item);
//         temp = getMaxDepthChild(dependencyMap, item, depth + 1, processedList);
//         // console.log("\t\t".repeat(depth) + "d=" + temp);
//         if (temp > depth) {
//           depth = temp;
//         }
//       }
//     }
//     return depth;
//   } else {
//     return depth;
//   }
// }

var nodeLinks;

function checkLinkPossible(fromNode, toNode, level, processedList) {
	if (processedList.indexOf(fromNode) != -1) {
		return level;
	}
	processedList.push(fromNode);
	// console.log('checkLinkPossible ,' + toNode + ' = ' + level);
	var dlls = dependencyMap.get(fromNode);
	// console.log(dlls);
	if (dlls != null) {
		var temp = -9999999;
		for (var x = 0; x < dlls.length; x++) {
			if (toNode == dlls[x]) {
				if (level > temp) {
					temp = level;
				}
			}
			if (levelMap.get(dlls[x]) < levelMap.get(toNode)) {
				var t = level + checkLinkPossible(dlls[x], toNode, level, processedList.slice(0));
				if (t > temp) {
					temp = t;
				}
			}
		}
		return temp;
	}
	return level;
}

function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

function drawNodelink(dll, processedList) {
	if (processedList.indexOf(dll) == -1) {
		processedList.push(dll);
		// var fromNode = `"${dll} (${levelMap.get(dll)})"`;
		var fromNode = `"${dll}"`;
		nodeLinks += `${fromNode} [style=filled, fillcolor="#cdcdcd"];\n`;
		var childList = dependencyMap.get(dll);
		if (childList != null) {
			for (var x = 0; x < childList.length; x++) {
				var item = childList[x];
				//console.log("item=" + item + '\t=>\t' + processedList);
				// if (processedList.indexOf(`"${dll}" -> "${item}"`)==-1){
				// var toNode = `"${item} (${levelMap.get(item)})"`;
				var toNode = `"${item}"`;

				console.log(dll + '==' + item + ' => ' + checkLinkPossible(dll, item, levelMap.get(item), []) + '==' + levelMap.get(item));
				if (checkLinkPossible(dll, item, levelMap.get(item), []) == levelMap.get(item) && levelMap.get(dll) <= levelMap.get(item)) {
					// if (levelMap.get(dll) < levelMap.get(item)) {
					nodeLinks += `        ${fromNode} -> ${toNode} [width=1, color="${getRandomColor()}", arrowhead=normal];\n`;
					// }
				} else {
					console.log('\t\t\t' + dll + '==' + item + ' => ' + checkLinkPossible(dll, item, levelMap.get(item), []) + '==' + levelMap.get(item));
				}
				// }
				drawNodelink(item, processedList);
			}
		}
	}
}

const argv = yargs
	.command('dot <path> [dll]', 'Generate the chart by dot command')
	.help()
	.alias('help', 'h')
	.argv;

if (process.argv.length - 2 == 0) {
	yargs.showHelp()
} else if (argv._.includes('dot')) {
	console.log('path = ' + argv.path);
	console.log('dll = ' + argv.dll);
	var fs = require("fs");

	// var walk = function (dir) {
	// 	var results = [];
	// 	var list = fs.readdirSync(dir);
	// 	list.forEach(function (file) {
	// 		file = dir + '/' + file;
	// 		var stat = fs.statSync(file);
	// 		if (stat && stat.isDirectory()) {
	// 			/* Recurse into a subdirectory */
	// 			results = results.concat(walk(file));
	// 		} else {
	// 			/* Is a file */
	// 			results.push(file);
	// 		}
	// 	});
	// 	return results;
	// }

	// var files = walk(argv.path);
	// files = files.filter(function (elem) {
	// 	return elem.endsWith('.dll');
	// })

	// cache
	// var cache = new Map();
	// for (var x = 0; x < files.length; x++) {
	// 	var file = files[x];
	// 	try {
	// 		console.log(`${x + 1}/${files.length} : ${file}`);
	// 		// console.log(`/Users/peter/workspace/coreclr/bin/Product/OSX.x64.Debug/ildasm -VISIBILITY=asm ${file}`);
	// 		var result = execSync(`/Users/peter/workspace/coreclr/bin/Product/OSX.x64.Debug/ildasm -VISIBILITY=asm ${file}`, {
	// 			stdio: [0]
	// 		}).toString();
	// 		cache.set(file, result);
	// 	} catch (err) {
	// 		// console.error('err=' + err);
	// 	}
	// }

	// first pass, get all assembly names per dll
	var result = execSync(`cd readAssembly; dotnet run namespace ${argv.path}`, {
		stdio: [0]
	}).toString();
	var dlls = result.split("\n");
	var assemblyMap = new Map();
	for (var x = 0; x < dlls.length; x++) {
		if (dlls[x] == '') {
			break;
		}
		var temp = dlls[x].split('----');
		//console.log(temp[0] + '\t' + temp[1] + '\n');
		assemblyMap.set(temp[1], temp[0]);
	}
	console.log(assemblyMap);
	// process.exit(0);

	// second pass, find out which dll is depend on which dll
	result = execSync(`cd readAssembly; dotnet run extern ${argv.path}`, {
		stdio: [0]
	}).toString();
	var mappings = result.split("\n");
	var dependencyMap = new Map();
	for (var x = 0; x < mappings.length; x++) {
		if (mappings[x] == '') {
			break;
		}
		console.log(mappings[x]);
		var temp = mappings[x].split('----');
		var file = temp[0];
		var assemblies = temp[1].split(',');
		var lines = [];
		for (y = 0; y < assemblies.length; y++) {
			// console.log('assemblies[y]=' + assemblies[y] + ',' + findDll(assemblyMap, assemblies[y]));
			var namespace = findDll(assemblyMap, assemblies[y]);
			if (namespace != null) {
				lines.push(namespace);
			}
		}
		dependencyMap.set(file, lines);
	}
	console.log(dependencyMap);
	// process.exit(0);

	// calculate level
	var levelMap = new Map();
	function setLevel(item, level, processedList) {
		var a = processedList.slice(0)
		console.log(`\t${item}, ${a} = ${a.indexOf(item)}, level=${level}`);
		if (levelMap.get(item) == null || level > levelMap.get(item)) {
			levelMap.set(item, level);
		}
		if (a.indexOf(item) != -1) {
			console.log('\t\treturn');
			return;
		} else {
			a.push(item);
		}
		var dlls = dependencyMap.get(item);
		// console.log(`\t${dlls}`);
		if (dlls != null) {
			for (var x = 0; x < dlls.length; x++) {
				setLevel(dlls[x], level + 1, a);
			}
		}
		console.log('\t\treturn');
	}
	if (argv.dll == null) {
		for (var [key, value] of assemblyMap) {
			// console.log(value);
			setLevel(value, 0, []);
		}
	} else {
		setLevel(argv.dll, 0, []);
	}
	console.log(levelMap);
	// process.exit(0);

	// draw whole dependency map
	// var nodeLinks = '';
	// for (var [key, value] of dependencyMap) {
	// 	nodeLinks += `"${key}" [style=filled, fillcolor="#ff7b3d"];\n`;
	// 	for (var x = 0; x < value.length; x++) {
	// 		nodeLinks += `        "${key}" -> "${value[x]}" [width=1, color="#b2562a", arrowhead=none];\n`;
	// 	}
	// }

	// draw nodelinks by specific dll
	nodeLinks = '';
	if (argv.dll == null) {
		var processedList = [];
		for (var [key, value] of assemblyMap) {
			// console.log(value);
			drawNodelink(value, processedList);
		}
	} else {

		drawNodelink(argv.dll, []);
	}

	// var maxDepthOfTree = getMaxDepth(dependencyMap);
	// console.log(`maxDepthOfTree=${maxDepthOfTree}`);

	var level = '';
	var levelNodes = '';
	for (x = 0; x < 100; x++) {
		var noOfElement = 0;
		for (var [key, value] of levelMap) {
			if (x == value) {
				noOfElement++;
			}
		}
		if (noOfElement > 0) {
			if (level != '') {
				level += ` -> `;
			}
			level += `"${x}"`;

			var nodes = '';
			for (var [key, value] of levelMap) {
				if (x == value) {
					nodes += `"${key}";`;
				}
			}
			levelNodes += `
				{
					rank = same; "${x}"; ${nodes}
				}
			`;

		}
	}
	// {
	// "Level 16.0" -> "Level 15.0" -> "Level 14.0" -> "Level 13.0" -> "Level 12.0" -> "Level 11.0" -> "Level 10.0" -> "Level 9.0" -> "Level 8.0" -> "Level 7.0" -> "Level 6.0" -> "Level 5.0" -> "Level 4.0" -> "Level 3.0" -> "Level 2.0" -> "Level 1.0" -> "Level 0.0"
	// }


	data = `
		digraph "[${argv.path} (${argv.dll})]"{
			margin = 0.7;
			fontname = Verdana;
			fontsize = 12;
			fontsize = 12;
			splines=ortho;
			nodesep=0.15;
			ranksep=0.15;
			node [shape=box, margin=0.04, shape=box, fontname="Ubuntu-M", fontsize = 12, width=0.2, height=0.1];
			{
				${level}
			}
			${levelNodes}
			${nodeLinks}
		}
	`;

	// console.log(dependencyMap);
	fs.writeFile("temp.dot", data, (err) => {
		if (err) console.log(err);
		console.log("done");
	});
}
